from django.shortcuts import render
from .models import EmployeeData
from .serializers import EmployeeDataSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework import viewsets


class EmployeeView(APIView):

    def get(self, request):
        queryset = EmployeeData.objects.all()
        serializer = EmployeeDataSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request):
        emp_name = request.data['emp_name']
        salary = request.data['salary']
        hours = request.data['hours']
        total_sal = salary * hours
        employee = EmployeeData.objects.create(emp_name=emp_name, sal_per_hr=salary, total_work_hrs=hours, total_sal=total_sal)
        
        serializer = EmployeeDataSerializer(data=employee)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response("", status=status.HTTP_201_CREATED)


class EmployeeDetailView(APIView):

    def get(self, request, pk):
        try:
            emp = EmployeeData.objects.get(emp_id=pk)
        except EmployeeData.DoesNotExist:
            return Response("The employee does not exist, please try again!")
        serializer = EmployeeDataSerializer(emp)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk):
        try:
            emp = EmployeeData.objects.get(emp_id=pk)
        except EmployeeData.DoesNotExist:
            return Response("The employee does not exist, please try again!")
        emp_name = request.data['emp_name']
        salary = request.data['salary']
        hours = request.data['hours']
        total_sal = salary * hours

        emp.emp_name=emp_name
        emp.sal_per_hr=salary 
        emp.total_work_hrs=hours
        emp.total_sal=total_sal
        emp.save()
        serializer = EmployeeDataSerializer(data=emp)
        if serializer.is_valid():
            return Response("", status=status.HTTP_200_OK)
        return Response(serializer.errors)

    def patch(self, request, pk):
        try:
            emp = EmployeeData.objects.get(pk=pk)
        except EmployeeData.DoesNotExist:
            return Response("The employee does not exist, please try again!")
        emp_name = request.data.get('emp_name', emp.emp_name)
        salary = request.data.get('salary', emp.sal_per_hr)
        hours = request.data.get('hours', emp.total_work_hrs)
        total_sal = salary * hours
        emp.emp_name=emp_name
        emp.sal_per_hr=salary 
        emp.total_work_hrs=hours
        emp.total_sal=total_sal
        emp.save()
        serializer = EmployeeDataSerializer(data=emp)
        if serializer.is_valid():
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors)
    


    def delete(self, request, pk):
        try:
            emp = EmployeeData.objects.get(pk=pk)
        except EmployeeData.DoesNotExist:
            return Response("The employee does not exist, please try again!")
        emp.delete()
        return Response("Employee deleted.")

