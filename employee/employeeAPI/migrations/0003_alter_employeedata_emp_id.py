# Generated by Django 4.1.2 on 2022-11-21 17:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('employeeAPI', '0002_remove_employeedata_id_alter_employeedata_emp_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='employeedata',
            name='emp_id',
            field=models.AutoField(primary_key=True, serialize=False, unique=True),
        ),
    ]
