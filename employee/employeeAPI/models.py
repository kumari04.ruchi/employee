from django.db import models

# Create your models here.

class EmployeeData(models.Model):
    emp_id = models.AutoField(unique=True, primary_key=True)
    emp_name = models.CharField(max_length=255)
    sal_per_hr = models.DecimalField(max_digits=20, decimal_places=2)
    total_work_hrs = models.IntegerField()
    total_sal = models.DecimalField(max_digits=20, decimal_places=2)

    def __str__(self):
        return self.emp_name
